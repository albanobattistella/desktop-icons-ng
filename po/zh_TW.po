# Chinese (Taiwan) translation for desktop-icons.
# Copyright (C) 2018 desktop-icons's COPYRIGHT HOLDER
# This file is distributed under the same license as the desktop-icons package.
# Yi-Jyun Pan <pan93412@gmail.com>, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: desktop-icons master\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-09 21:00+0100\n"
"PO-Revision-Date: 2021-07-11 21:08+0800\n"
"Last-Translator: pan93412 <pan93412@gmail.com>\n"
"Language-Team: Chinese (Taiwan) <chinese-l10n@googlegroups.com>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.4.3\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: askRenamePopup.js:42
msgid "Folder name"
msgstr "資料夾名稱"

#: askRenamePopup.js:42
msgid "File name"
msgstr "檔案名稱"

#: askRenamePopup.js:49 desktopManager.js:811
msgid "OK"
msgstr "確定"

#: askRenamePopup.js:49
msgid "Rename"
msgstr "重新命名"

#: desktopIconsUtil.js:88
msgid "Command not found"
msgstr "無法找到指令"

#: desktopIconsUtil.js:225
msgid "Do you want to run “{0}”, or display its contents?"
msgstr "要執行「{0}」，還是顯示它的內容？"

#: desktopIconsUtil.js:226
msgid "“{0}” is an executable text file."
msgstr "「{0}」是可執行的文字檔。"

#: desktopIconsUtil.js:230
msgid "Execute in a terminal"
msgstr "於終端機執行"

#: desktopIconsUtil.js:232
msgid "Show"
msgstr "顯示"

#: desktopIconsUtil.js:234 desktopManager.js:813 fileItemMenu.js:338
msgid "Cancel"
msgstr "取消"

#: desktopIconsUtil.js:236
msgid "Execute"
msgstr "執行"

#: desktopManager.js:199
msgid "Nautilus File Manager not found"
msgstr "無法找到《檔案 (Nautilus 瀏覽器)》"

#: desktopManager.js:200
msgid "The Nautilus File Manager is mandatory to work with Desktop Icons NG."
msgstr "《檔案》強制與Desktop Icons NG 運行"

#: desktopManager.js:774
msgid "Clear Current Selection before New Search"
msgstr ""

#: desktopManager.js:815
msgid "Find Files on Desktop"
msgstr ""

#: desktopManager.js:875 desktopManager.js:1520
msgid "New Folder"
msgstr "新增資料夾"

#: desktopManager.js:879
msgid "New Document"
msgstr "新增文件"

#: desktopManager.js:884
msgid "Paste"
msgstr "貼上"

#: desktopManager.js:888
msgid "Undo"
msgstr "復原"

#: desktopManager.js:892
msgid "Redo"
msgstr "重做"

#: desktopManager.js:898
#, fuzzy
msgid "Select All"
msgstr "全部選取"

#: desktopManager.js:906
#, fuzzy
msgid "Show Desktop in Files"
msgstr "在《檔案》中開啟桌面"

#: desktopManager.js:910 fileItemMenu.js:267
msgid "Open in Terminal"
msgstr "開啟終端器"

#: desktopManager.js:916
msgid "Change Background…"
msgstr "變更背景圖片…"

#: desktopManager.js:925
msgid "Display Settings"
msgstr "顯示設定"

#: desktopManager.js:932
#, fuzzy
msgid "Desktop Icons Settings"
msgstr "桌面圖示設定"

#: desktopManager.js:1578
msgid "Arrange Icons"
msgstr ""

#: desktopManager.js:1582
msgid "Arrange By..."
msgstr ""

#: desktopManager.js:1591
msgid "Keep Arranged..."
msgstr ""

#: desktopManager.js:1595
msgid "Keep Stacked by type..."
msgstr ""

#: desktopManager.js:1600
msgid "Sort Home/Drives/Trash..."
msgstr ""

#: desktopManager.js:1606
msgid "Sort by Name"
msgstr ""

#: desktopManager.js:1608
msgid "Sort by Name Descending"
msgstr ""

#: desktopManager.js:1611
msgid "Sort by Modified Time"
msgstr ""

#: desktopManager.js:1614
msgid "Sort by Type"
msgstr ""

#: desktopManager.js:1617
msgid "Sort by Size"
msgstr ""

#. TRANSLATORS: "Home" is the text that will be shown in the user's personal folder
#: fileItem.js:136
msgid "Home"
msgstr "家目錄"

#: fileItemMenu.js:105
msgid "Open All..."
msgstr "開啟所有項目……"

#: fileItemMenu.js:105
msgid "Open"
msgstr "開啟"

#: fileItemMenu.js:116
msgid "Stack This Type"
msgstr ""

#: fileItemMenu.js:116
msgid "Unstack This Type"
msgstr ""

#: fileItemMenu.js:128
msgid "Scripts"
msgstr "命令稿"

#: fileItemMenu.js:134
msgid "Open All With Other Application..."
msgstr "用其他應用程式開啟所有……"

#: fileItemMenu.js:134
msgid "Open With Other Application"
msgstr "用其他應用程式開啟"

#: fileItemMenu.js:140
msgid "Launch using Dedicated Graphics Card"
msgstr ""

#: fileItemMenu.js:150
msgid "Cut"
msgstr "剪下"

#: fileItemMenu.js:155
msgid "Copy"
msgstr "複製"

#: fileItemMenu.js:161
msgid "Rename…"
msgstr "重新命名…"

#: fileItemMenu.js:169
msgid "Move to Trash"
msgstr "移動到垃圾桶"

#: fileItemMenu.js:175
msgid "Delete permanently"
msgstr "永久刪除"

#: fileItemMenu.js:183
msgid "Don't Allow Launching"
msgstr "不允許執行"

#: fileItemMenu.js:183
msgid "Allow Launching"
msgstr "允許執行"

#: fileItemMenu.js:194
msgid "Empty Trash"
msgstr "清空垃圾桶"

#: fileItemMenu.js:205
msgid "Eject"
msgstr "退出"

#: fileItemMenu.js:211
msgid "Unmount"
msgstr "卸載"

#: fileItemMenu.js:221
msgid "Extract Here"
msgstr "在這裡解開"

#: fileItemMenu.js:225
msgid "Extract To..."
msgstr "解開到…"

#: fileItemMenu.js:232
msgid "Send to..."
msgstr "傳送到…"

#: fileItemMenu.js:238
msgid "Compress {0} file"
msgid_plural "Compress {0} files"
msgstr[0] "壓縮 {0} 檔案"

#: fileItemMenu.js:244
#, fuzzy
msgid "New Folder with {0} item"
msgid_plural "New Folder with {0} items"
msgstr[0] "新增空白的（有 {0} 項目）資料夾"

#: fileItemMenu.js:253
msgid "Common Properties"
msgstr "共同屬性"

#: fileItemMenu.js:253
msgid "Properties"
msgstr "屬性"

#: fileItemMenu.js:260
msgid "Show All in Files"
msgstr "在《檔案》中顯示"

#: fileItemMenu.js:260
msgid "Show in Files"
msgstr "在《檔案》中顯示"

#: fileItemMenu.js:336
msgid "Select Extract Destination"
msgstr "選擇解壓縮的位置"

#: fileItemMenu.js:339
msgid "Select"
msgstr "選擇"

#: fileItemMenu.js:366
msgid "Can not email a Directory"
msgstr "無法寄送資料夾"

#: fileItemMenu.js:367
msgid "Selection includes a Directory, compress the directory to a file first."
msgstr "已選項目含資料夾，先把資料夾壓縮成一個檔案。"

#: preferences.js:91
msgid "Settings"
msgstr "設定"

#: preferences.js:98
msgid "Size for the desktop icons"
msgstr "桌面圖示的大小"

#: preferences.js:98
msgid "Tiny"
msgstr "更小圖示"

#: preferences.js:98
msgid "Small"
msgstr "小圖示"

#: preferences.js:98
msgid "Standard"
msgstr "適中圖示"

#: preferences.js:98
msgid "Large"
msgstr "大圖示"

#: preferences.js:99
msgid "Show the personal folder in the desktop"
msgstr "在桌面顯示個人資料夾"

#: preferences.js:100
msgid "Show the trash icon in the desktop"
msgstr "在桌面顯示垃圾桶圖示"

#: preferences.js:101 schemas/org.gnome.shell.extensions.ding.gschema.xml:45
msgid "Show external drives in the desktop"
msgstr "在桌面顯示外接硬碟"

#: preferences.js:102 schemas/org.gnome.shell.extensions.ding.gschema.xml:50
msgid "Show network drives in the desktop"
msgstr "在桌面顯示網路資料夾"

#: preferences.js:105
msgid "New icons alignment"
msgstr "新圖示排序位置"

#: preferences.js:106
msgid "Top-left corner"
msgstr "左上角"

#: preferences.js:107
msgid "Top-right corner"
msgstr "右上角"

#: preferences.js:108
msgid "Bottom-left corner"
msgstr "左下角"

#: preferences.js:109
msgid "Bottom-right corner"
msgstr "右下角"

#: preferences.js:111 schemas/org.gnome.shell.extensions.ding.gschema.xml:55
msgid "Add new drives to the opposite side of the screen"
msgstr "將新掛載硬碟置於螢幕的另一邊"

#: preferences.js:112
msgid "Highlight the drop place during Drag'n'Drop"
msgstr "拖曳時強調顯示拖曳後位置"

#: preferences.js:116
msgid "Settings shared with Nautilus"
msgstr "與《檔案》共用設定"

#: preferences.js:122
msgid "Click type for open files"
msgstr "開啟檔案時的點擊方式"

#: preferences.js:122
msgid "Single click"
msgstr "單擊"

#: preferences.js:122
msgid "Double click"
msgstr "雙擊"

#: preferences.js:123
msgid "Show hidden files"
msgstr "顯示隱藏檔"

#: preferences.js:124
msgid "Show a context menu item to delete permanently"
msgstr "在快速選單顯示永久刪除"

#: preferences.js:129
msgid "Action to do when launching a program from the desktop"
msgstr "在桌面執行應用程式時的動作"

#: preferences.js:130
msgid "Display the content of the file"
msgstr "顯示檔案內容"

#: preferences.js:131
msgid "Launch the file"
msgstr "執行檔案"

#: preferences.js:132
msgid "Ask what to do"
msgstr "詢問要做什麼"

#: preferences.js:138
msgid "Show image thumbnails"
msgstr "顯示影像縮圖"

#: preferences.js:139
msgid "Never"
msgstr "從不"

#: preferences.js:140
msgid "Local files only"
msgstr "顯示本機檔案"

#: preferences.js:141
msgid "Always"
msgstr "總是"

#: prefs.js:37
msgid ""
"To configure Desktop Icons NG, do right-click in the desktop and choose the "
"last item: 'Desktop Icons settings'"
msgstr "要設定Desktop Icons NG，在桌面按滑鼠右鍵並選擇最後項目「桌面圖示設定」"

#: showErrorPopup.js:37
msgid "Close"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:25
msgid "Icon size"
msgstr "圖示大小"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:26
msgid "Set the size for the desktop icons."
msgstr "設定桌面圖示的大小。"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:30
msgid "Show personal folder"
msgstr "顯示個人資料夾"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:31
msgid "Show the personal folder in the desktop."
msgstr "在桌面顯示個人資料夾。"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:35
msgid "Show trash icon"
msgstr "顯示垃圾桶圖示"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:36
msgid "Show the trash icon in the desktop."
msgstr "在桌面顯示垃圾桶圖示。"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:40
msgid "New icons start corner"
msgstr "新圖示排序方式"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:41
msgid "Set the corner from where the icons will start to be placed."
msgstr "設定新圖示置放的位置"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:46
msgid "Show the disk drives connected to the computer."
msgstr "顯示連接於本機的硬碟"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:51
#, fuzzy
msgid "Show mounted network volumes in the desktop."
msgstr "在桌面顯示個人資料夾。"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:56
msgid ""
"When adding drives and volumes to the desktop, add them to the opposite side "
"of the screen."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:60
msgid "Shows a rectangle in the destination place during DnD"
msgstr "拖曳同時在拖曳目的位置顯示矩形方格"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:61
msgid ""
"When doing a Drag'n'Drop operation, marks the place in the grid where the "
"icon will be put with a semitransparent rectangle."
msgstr "當執行拖曳，在圖示即將被置放的位置用半透明矩形顯示方格"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:65
msgid "Sort Special Folders - Home/Trash Drives."
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:66
msgid ""
"When arranging Icons on desktop, to sort and change the position of the "
"Home, Trash and mounted Network or External Drives"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:70
msgid "Keep Icons Arranged"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:71
msgid "Always keep Icons Arranged by the last arranged order"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:75
msgid "Arrange Order"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:76
msgid "Icons Arranged by this property"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:80
msgid "Keep Icons Stacked"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:81
msgid "Always keep Icons Stacked, Similar types are grouped"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:85
msgid "Type of Files to not Stack"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:86
msgid "An Array of strings types, Don't Stack these types of files"
msgstr ""

#~ msgid "New folder"
#~ msgstr "新增資料夾"

#~ msgid "Delete"
#~ msgstr "刪除"

#~ msgid "Error while deleting files"
#~ msgstr "刪除檔案時發生錯誤"

#~ msgid "Are you sure you want to permanently delete these items?"
#~ msgstr "你確定要永久刪除這些項目嗎？"

#~ msgid "If you delete an item, it will be permanently lost."
#~ msgstr "如果直接刪除本項目，它會永久消失。"

#, fuzzy
#~ msgid "Show external disk drives in the desktop"
#~ msgstr "在桌面顯示個人資料夾"

#, fuzzy
#~ msgid "Show the external drives"
#~ msgstr "在桌面顯示個人資料夾"

#, fuzzy
#~ msgid "Show network volumes"
#~ msgstr "在《檔案》中顯示"

#~ msgid "Huge"
#~ msgstr "巨大圖示"
